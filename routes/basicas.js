const express = require('express')
const router = express.Router()
const fetch = require('node-fetch')


router.get('/detalle/:idPelicula', (req, res)=>{
  const id = req.params.idPelicula
  console.log(id)
  fetch(`http://www.omdbapi.com/?apikey=28e40b98&i=${id}`)
    .then(data=>data.json())
    .then(data=>{
      res.render('detalle', data)
    })
})



router.route('/search')
  .get((req, res)=> res.render('search'))
  .post((req,res)=>{
    fetch(`http://www.omdbapi.com/?apikey=28e40b98&s=${req.body.busqueda}`)
      .then(data=>data.json())
      .then(data=>res.render('listado',{
        title: 'Resultado de busqueda',
        peliculas: data.Search
      }))
  })


router.get('*', (req, res)=>res.send('404'))

module.exports = router

const express = require('express')
const app = express()
const path=require('path')
const routsBasicas = require('./routes/basicas')


app.set('view engine', 'pug')
app.set('views', './views')

app.use(express.urlencoded({ extended: false }))


app.use(express.static('public'))

app.use(routsBasicas)



app.listen(3000, ()=> console.log('servidor escuchando el puerto 3000'))
